//
//  ViewController.swift
//  ExtensionTutorial
//
//  Created by iffytheperfect on 8/4/14.
//  Copyright (c) 2014 iffytheperfect. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
                            
    @IBOutlet weak var myTextField: UITextField!
    @IBOutlet weak var displayLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Colors
    
    @IBAction func goldButtonPressed(sender: AnyObject) {
        
        displayLabel.text = myTextField.text
        displayLabel.textColor = UIColor().goldColor()
    }

    @IBAction func saddleBrownPressed(sender: AnyObject) {
        displayLabel.text = myTextField.text
        displayLabel.textColor = UIColor().saddleBrown()
    }
    
    
    // Int
    
    @IBAction func plus3Pressed(sender: AnyObject) {
        
        let number: Int = myTextField.text.toInt()!
        
        displayLabel.text = String(number.plus3)
    }
    
    @IBAction func multiplyBy2(sender: AnyObject) {
        
        let number: Int = myTextField.text.toInt()!
        
        displayLabel.text = String(number.multiplyBy2)
    }
    
    
}

