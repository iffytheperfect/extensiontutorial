//
//  IntUtility.swift
//  ExtensionTutorial
//
//  Created by iffytheperfect on 8/4/14.
//  Copyright (c) 2014 iffytheperfect. All rights reserved.
//

import Foundation

extension Int
{
    var plus3: Int
    {
        return self + 3
    }
    
    var multiplyBy2: Int
    {
        return self * 2
    }
}