//
//  UIColorUtility.swift
//  ExtensionTutorial
//
//  Created by iffytheperfect on 8/4/14.
//  Copyright (c) 2014 iffytheperfect. All rights reserved.
//

import Foundation
import UIKit

extension UIColor
{
    func goldColor() -> UIColor
    {
        return UIColor(red: 255.0 / 255.0, green: 215.0 / 255.0, blue: 0.0 / 255.0, alpha: 1.0)
    }
    
    func saddleBrown() -> UIColor
    {
        return UIColor(red: 139.0 / 255.0, green: 69.0 / 255.0, blue: 19.0 / 255.0, alpha: 1.0)
    }
    
    func blackColor2() -> UIColor
    {
        return UIColor.blackColor()
    }
    
    func greenColor2() -> UIColor
    {
        return UIColor.greenColor()
    }

    func blueColor2() -> UIColor
    {
        return UIColor.blueColor()
    }
    
    func brownColor2() -> UIColor
    {
        return UIColor.brownColor()
    }
}